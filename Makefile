.PHONY: deploy
deploy: book
	@echo "====> deploying to github"
	if [ ! -d "/tmp/notes-book" ]; then mkdir -p /tmp/notes-book; fi
	rm -r /tmp/notes-book/
	git worktree prune
	git worktree add /tmp/notes-book/ main
	rm -rf /tmp/notes-book/*
	cp -rp book/* /tmp/notes-book/
	cd /tmp/notes-book &&\
		git add -A &&\
		git commit -m "deployed on $(shell date) by ${USER}" &&\
		git push github main -f
