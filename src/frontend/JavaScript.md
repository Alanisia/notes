# JavaScript

***TODO***

## 对象

***TODO***

## 原型链

***TODO***

## `Promise`

***TODO***

## `this`

***TODO***

### `apply` & `call` & `bind`

***TODO***

## DOM & BOM

***TODO***

### 事件

***TODO***

### 动画

***TODO***

#### 定时器

***TODO***

#### `requestAnimationFrame(callback)`

[https://developer.mozilla.org/zh-CN/docs/Web/API/Window/requestAnimationFrame](https://developer.mozilla.org/zh-CN/docs/Web/API/Window/requestAnimationFrame)

请求动画帧，是一个浏览器的宏任务，其间隔时间由浏览器自身决定。

`requestAnimationFrame(callback)`是全局`window`对象的一个属性函数，返回值为一个整数，用于定时器的身份标识，可以使用`window.cancelAnimationFrame()`来取消回调函数执行。

函数接收的参数`callback`是一个函数，即下一次重绘之前更新动画帧所调用的函数。若想在浏览器下次重绘前继续更新下一帧动画，回调函数自身需要再次调用`requestAnimationFrame(callback)`（即需要写成递归的形式）。

比定时器好的地方：***TODO***

### 节流与防抖

当函数绑定一些持续触发的事件（如resize、scroll、mousemove、键盘输入、多次快速click等），若每次触发都要执行一次函数则会带来性能下降，资源请求频繁等问题，此时需要应用节流与防抖。

#### 防抖

指触发事件n秒后才执行函数，如果在n秒内又触发事件则会重新计算函数执行时间。

```javascript
const debounce = (func, delay, immediate) => {
	let timer = null;
	return () => {
		if (timer) clearInterval(timer);
		if (immediate) {
			const 
	};
};
```

#### 节流

指连续触发事件但在n秒中只执行一次函数。节流会稀释函数的执行频率。

```javascript
const throttle = (func, delay) => {
	let timer = null;
	return () => {
		if (!timer) {
			timer = setTimeout(() => {
				timer = null;
				func();
			}, delay);
		}
	};
};
```
