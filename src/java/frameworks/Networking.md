# 网络

## 网络I/O框架与Web容器的比较

- 网络I/O框架直接封装传输层的TCP/UDP协议，而Web容器是基于Servlet的，Servlet 3.0基于传输层，封装了应用层协议，包括HTTP；
- 网络I/O框架无需Web容器的支持，可以直接在程序中应用这些框架实现客户端和服务器通信，而Web容器一般部署在服务器端，对Servlet 3.0提供了不同的实现。

## Netty

一款基于NIO（Nonblocking IO）开发的网络通信框架，对比BIO（Blocking IO），并发性能得到很大的提高，在快速和易用性的同时并未丧失可维护性和性能等优势。

特点：高并发、传输快、封装好

重要组件：

- Channel：Netty的网络操作接口，包括基本的IO操作如`bind()`、`connect()`、`read()`、`write()`等；常用的Channel接口实现类有`NioServerSocketChannel`和`NioSocketChannel`；
- EventLoop：定义了Netty的核心抽象，用于处理连接的生命周期中所发生的事件，即其主要作用为负责监听网络事件并调用事件处理器进行IO相关操作的处理；
- ChannelFuture：由于Netty是异步非阻塞的，无法立刻得到操作结果，可以通过该接口的`addListener()`注册一个`ChannelFutureListener`进行监听；
- ChannelHandler：消息的具体处理器，负责处理读写操作、客户端连接等；
- ChannelPipeline：ChannelHandler的链，提供了一个容器并定义了用于沿着链传播入站和出站时间流的API，当Channel被创建时，它被自动分配到它专属的ChannelPipeline；可以在ChannelPipeline上通过`addLast()`方法添加一个或多个ChannelHandler，因为一个数据或者事件可能会被多个handler处理，当一个ChannelHandler处理完数据之后就交给下一个ChannelHandler；
- Bootstrap/ServerBootstrap：前者是客户端的启动/引导类，后者是服务端的启动/引导类，Bootstrap只需配置一个线程组，ServerBootstrap需要配置两个，一个用于接受连接，一个用于具体的处理。

### `Channel`

`Channel`生命周期：

1. `channelRegistered`：Channel注册到了一个EventLoop
2. `channelActive`：变为活跃状态，连接到了远程主机，可以接收和发送数据
3. `channelInactive`：Channel处于非活跃状态，没有连接到远程主机
4. `channelUnregistered`：Channel已经创建但是未注册到一个EventLoop里面，即未和Selector绑定

### `ChannelHandler`

主要是两个子接口：

- `ChannelInboundHandler`：处理输入数据和Channel状态类型改变，适配器：`ChannelInboundHandlerAdapter`

- `ChannelOutboundHandler`：处理输出数据，适配器：`ChannelOutboundHandlerAdapter`

> **适配器模式的应用**
> 
> 上面两个接口有很多默认方法需要实现，采用适配器模式，用`ChannelInboundHandlerAdapter`和`ChannelOutboundHandlerAdapter`去实现接口，自定义ChannelHandler时只需要继承适配器，无须重写上面接口的所有方法。

`ChannelHandler`生命周期：

1. `handlerAdded`：当ChannelHandler添加到ChannelPipeline时调用
2. `handlerRemoved`：当ChannelHandler从ChannelPipeline移除时调用
3. `exceptionCaught`：当ChannelPipeline执行抛出异常时调用

### `ChannelPipeline`

ChannelHandler实例对象的链表，用于处理或截获通道的接收和发送数据。

入站出站ChannelHandler的执行顺序：

- 入站顺序执行，出站逆序执行
- 客户端先执行出站之后再执行入站，服务端相反
- InboundHandler通过`ctx.write(msg)`会将数据传递给OutboundHandler，此时InboundHandler需要放在结尾否则OutboundHandler会不执行；但使用`channel.write(msg)`和`pipeline.write(msg)`时都会执行（channel和pipeline会贯穿整个流）

### Netty线程模型

基于主从Reactors多线程模型，Netty对此作了修改，其中主从Reactor多线程模型有多个Reactor：

- MainReactor：负责客户端的连接请求，并将请求转发给SubReactor
- SubReactor：负责相应通道的IO读写请求
- 非IO请求（具体逻辑处理）的任务会直接写入队列，等待worker threads进行处理

```java
EventLoopGroup bossGroup = new NioEventLoopGroup();
EventLoopGroup workGroup = new NioEventLoopGroup();
```

以上代码中，`bossGroup`和`workGroup`均是线程池，

- `bossGroup`线程池绑定某个端口后获得其中一个线程作为MainReactor，专门处理端口的accept事件，每个端口对应一个boss线程
- `workGroup`线程池会被各个SubReactor和worker线程充分利用

_EventLoopGroup与EventLoop关系：EventLoopGroup包含多个EventLoop（每个EventLoop通常包含一个线程），EventLoop处理的事件都将在它专有的线程上处理，从而保证线程安全。_

### 异步处理

Netty的IO操作是异步的，包括`bind()`，`connect()`，`write()`等操作会简单地返回一个`ChannelFuture`，调用者不能立刻获得结果，通过Future-Listener机制，用户可以方便地主动获取或者通过通知机制获得IO操作结果。

相比传统阻塞IO，异步处理的好处是不会造成线程阻塞，线程在IO操作期间可以执行别的程序，在高并发情形下会更稳定和更高的吞吐量。

### `ByteBuf`

***TODO***

### 拆包与粘包处理

Netty对解决拆包粘包问题做了抽象，提供一些解码器来解决拆包粘包问题，如：

- `LineBasedFrameDecoder`：以行为单位（换行符）进行数据包的解码
- `DelimiterBasedFrameDecoder`：以特殊符号作为分隔来进行数据包的解码
- `FixedLengthFrameDecoder`：以固定长度进行数据包的解码
- `LengthFieldBasedFrameDecoder`：适用于消息头包含消息长度的协议

也可以通过自定义序列化编解码器来解决拆包粘包问题。

### 零拷贝

操作系统层面的零拷贝通常指避免在用户态和内核态之间来回拷贝数据，Netty层面主要体现在对数据操作的优化：

- 使用了Netty提供的`CompositeByteBuf`类，可以将多个ByteBuf合并为一个逻辑上的ByteBuf，避免了各个ByteBuf的拷贝；
- ByteBuf支持slice操作，因此可以将ByteBuf分解为多个共享同一个存储区域的ByteBuf，避免内存拷贝；
- 通过`FileRegion`包装的`FileChannel.transferTo`实现文件传输，可以直接将文件缓冲区的数据发送到目标Channel，避免了传统通过循环write的方式导致的内存拷贝问题。

## Vert.x

***TODO***
