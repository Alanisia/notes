# Spring全家桶

## Spring

### IOC/DI

控制反转(Inverse of Control)/依赖注入(Dependency Injection)，将传统的程序代码直接操控的对象的调用权交给容器，通过容器来实现对象组件的装配和管理。

作用：

1. 管理对象的创建和依赖关系的维护
2. 解耦，由容器去维护具体的对象
3. 托管了类的产生过程

优点：

1. IOC能把代码量降低
2. 使应用容易测试，单元测试不再需要单例和JNDI查找机制
3. 最小的代价和最小的侵入性使松散耦合得以实现
4. IOC容器支持加载服务时的饿汉式初始化和懒加载

### ApplicationContext

`BeanFactory`负责配置、创建和管理Bean，许多功能需要通过编程实现；`ApplicationContext`由`BeanFactory`派生而来，可以通过配置方式实现功能，也称之为Spring上下文，负责管理Bean与Bean之间的依赖关系。

### Bean

#### 生命周期

1. Spring启动，查找并加载需要被Spring管理的Bean，进行Bean实例化；
2. Bean实例化后，对Bean的引用和值注入到Bean属性中；
3. 如果Bean实现了`BeanNameAware`接口，Spring将Bean的Id传递给`setBeanName()`方法；
4. 如果Bean实现了`BeanFactoryAware`接口，Spring将调用`setBeanFactory()`方法，将BeanFactory容器实例传入；
5. 如果Bean实现了`ApplicationContextAware`接口，Spring将调用Bean的`setApplicationContext()`方法，将Bean所在应用的上下文引用传入进来； 
6. 如果Bean实现了`BeanPostProcessor`接口，Spring将调用它们的`postProcessBeforeInitialization()`方法；
7. 如果Bean实现了`InitializingBean`接口，Spring将调用它们的`afterPropertiesSet()`方法，类似地，如果Bean使用init-method声明了初始化方法，该方法也会被调用；
8. 如果Bean实现了`BeanPostProcessor`接口，Spring将调用它们的`postProcessAfterInitialization()`方法；
9. 此时，Bean已经准备就绪，可以被应用程序使用了，它们将一直驻留在应用上下文中，直到应用上下文被销毁；
10. 如果Bean实现了`DispoableBean`接口，Spring将调用它的`destroy()`方法，同样，如果Bean使用了destroy-method声明销毁方法，该方法也会被调用。

#### `BeanPostProcessor`

[https://www.cnblogs.com/tuyang1129/p/12866484.html](https://www.cnblogs.com/tuyang1129/p/12866484.html)

要在容器中创建bean时执行一些自定义逻辑，可以创建一个类并实现`BeanPostProcessor`接口，之后会优先创建实现了`BeanPostProcessor`的bean；在创建其他bean时会将创建的bean作为参数调用`BeanPostProcessor`的方法。该接口包含两个方法：

```java
public interface BeanPostProcessor {
	default Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
		return bean;
	}

	default Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
		return bean;
	}
}
```

可以在容器中注册多个不同的`BeanPostProcessor`对象，在bean创建过程中将会轮流执行这些对象的before和after方法。Spring提供了一个`Ordered`接口用于确定多个`BeanPostProcessor`的执行顺序，`BeanPostProcessor`实现类实现`Ordered`接口并重写`getOrder`方法，`getOrder`返回值是一个整型，返回值越小的`BeanPostProcessor`的方法优先被执行。

#### 作用域

可使用`@Scope`注解声明Bean作用域，默认`singleton`。

1. singleton：唯一Bean实例，Spring中的Bean默认都是单例的；
2. prototype：每次请求都会创建一个新的Bean实例；
3. request：每一次HTTP请求都会产生一个新的Bean，该Bean仅在当前HTTP Request内有效；
4. session：每一次HTTP请求都会产生一个新的Bean，该Bean仅在当前HTTP Session内有效；
5. global-session：全局Session作用域，仅仅在基于portlet的web应用才有意义，Spring 5已废除portlet。Portlet是能够生成语义代码片段的小型Java Web插件，它们基于portlet容器，可以像servlet一样处理HTTP请求，但是与servlet不同，每个portlet都有不同的会话。

### 常用注入方式

- 构造器依赖注入
- Setter方法注入
- 基于属性注入

### 装配与自动装配

`@Autowired`与`@Resource`的区别：

1. `@Autowired`默认是按照类型装配注入的，默认情况下它要求依赖对象必须存在（可以设置其`required`属性为`false`）；
2. `@Resource`默认是按照名称来装配注入的，只有当找不到与名称匹配的bean才会按照类型来装配注入。

### 事务

#### 隔离级别

`TransactionDefinition`接口中定义了五个表示隔离级别的常量：

- `TransactionDefinition.ISOLATION_DEFAULT`：使用后端数据库默认的隔离级别
- `TransactionDefinition.ISOLATION_READ_UNCOMMITTED`：最低隔离级别，读取未提交
- `TransactionDefinition.ISOLATION_READ_COMMITTED`：读取已提交
- `TransactionDefinition.ISOLATION_REPEATABLE_READ`：可重复读
- `TransactionDefinition.ISOLATION_SERIALIZABLE`：最高隔离级别，串行化

#### 事务传播

[https://www.cnblogs.com/renxiuxing/p/15395798.html](https://www.cnblogs.com/renxiuxing/p/15395798.html)

Spring事务传播行为用来描述由某个事务传播行为修饰的方法被嵌套进另一个方法时事务如何传播，如：

```java
public void a() {}

@Transaction(Propagation=XXX)
public void b() {}
```

代码中`a()`嵌套调用了`b()`，`b()`的事务传播行为由`@Transaction(Propagation=XXX)`设置决定。_（注：`a()`没有开启事务，某个事务传播行为修饰的方法并不是必须要在开启事务的外围方法中调用。）_

1. `TransactionDefinition.PROPAGATION_REQUIRED`：如果当前没有事务，则新建一个事务，如果当前存在事务，就加入该事务
2. `TransactionDefinition.PROPAGATION_SUPPORTS`：支持当前事务，如果当前存在事务，就加入该事务，否则以非事务执行
3. `TransactionDefinition.PROPAGATION_MANDATORY`：支持当前事务，如果当前存在事务，就加入该事务，否则抛出异常
4. `TransactionDefinition.PROPAGATION_REQUIRES_NEW`：创建新事务，无论当前存不存在事务
5. `TransactionDefinition.PROPAGATION_NOT_SUPPORTED`：以非事务方式执行操作，若当前存在事务，就把当前事务挂起
6. `TransactionDefinition.PROPAGATION_NEVER`：以非事务方式执行操作，若当前存在事务，则抛出异常
7. `TransactionDefinition.PROPAGATION_NESTED`：如果当前存在事务，则在嵌套事务内执行；如果当前没有事务，则按REQUIRED属性执行

### 设计模式

- 工厂模式
- 代理模式
- 单例模式
- 模板模式
- 包装器模式
- 观察者模式
- 适配器模式

### AOP

AOP（Aspect-Oriented Programming，面向切面编程）能够将那些与业务无关，却为业务模块所共同调用的逻辑或责任（如事务处理、日志管理、权限控制等）抽取出来并进行封装，达到横切逻辑代码与业务逻辑代码分离的效果，便于减少系统的重复代码，降低模块间的耦合度，并有利于未来的可扩展性和可维护性。

AOP在不改变原有的业务逻辑下，增强横切逻辑代码，根本上解耦合，避免横切逻辑代码重复。

Spring AOP基于动态代理，若要代理的对象实现了某个接口，Spring AOP会使用JDK动态代理去创建代理对象，而对于没有实现接口的对象就无法使用JDK动态代理去代理了，此时Spring AOP会使用Cglib生成一个被代理对象的子类来进行代理。另外，Spring AOP已经集成AspectJ。

使用AOP后可以把一些通用功能抽象出来，在需要用到的地方直接使用即可，这样大大简化了代码量，在需要增加新功能时也方便，并且提高了系统扩展性。日志功能、事务管理等场景都用到了AOP。

#### AOP基本概念

- 切面(Aspect)：官方的抽象定义为“一个关注点的模块化，这个关注点可能会横切多个对象”
- 连接点(Joinpoint)：程序执行过程中的某个行为
- 通知(Advice)：“切面”对于某个“连接点”所产生的动作
- 切入点(Pointcut)：匹配连接点的断言，在AOP中通知和一个切入点表达式关联
- 目标对象(Target Object)：被一个或者多个切面所通知的对象
- 织入(Weaving)：将切面应用到目标对象并导致代理对象创建的过程
- AOP代理(AOP Proxy)：在Spring AOP中有两种代理方式，JDK动态代理和CGLIB代理

#### AOP通知类型

1. 前置通知
2. 后置通知
3. 环绕通知
4. 返回后通知
5. 抛出异常后通知

#### Spring AOP与AspectJ的区别

1. Spring AOP属于运行时增强，AspectJ属于编译时增强；
2. Spring AOP基于代理，AspectJ基于字节码操作；
3. AspectJ相比Spring AOP功能更强大，但Spring AOP相对来说更简单；
4. 切面较少时两者性能差异不大，切面太多时AspectJ比Spring AOP快。

## SpringMVC

MVC是Model-View-Controller的简称，是一种架构模式，它分离了表现和交互。被分为三个核心部分：

- 模型（Model）：***TODO***
- 视图（View）：***TODO***
- 控制器（Controller）：***TODO***

### Spring MVC工作原理

1. 客户端发送请求
2. 前端控制器DispatcherServlet接受用户请求
3. 找到处理器映射HandlerMapping解析请求对应的Handler
4. HandlerAdaptor会根据Handler来调用真正的处理器处理请求，并处理相应的业务逻辑
5. 处理器返回一个模型视图ModelAndView
6. 视图解析器进行解析
7. 返回一个视图对象
8. 前端控制器DispatcherServlet渲染数据（Model）
9. 将得到的视图对象返回给用户

### Spring MVC核心组件

- `DispatcherServlet`：前端控制器，相当于Spring MVC的中央处理器，是整个控制流程的中心，由它调用其它组件处理用户的请求，降低组件间的耦合性
- `HandlerMapping`：处理器映射器，根据请求的URL查找Handler（Controller）
- `HandlerAdaptor`：处理器适配器，按照特定规则（HandlerAdaptor要求的规则）去执行Handler，是适配器模式的应用，通过扩展适配器可以对更多类型的处理器进行执行
- `Handler`：处理器，编写具体的业务逻辑
- `ViewResolver`：视图解析器，进行视图解析，根据逻辑视图名解析成真正的视图（View），ViewResolver首先根据逻辑视图名解析成物理视图名即具体之页面地址，再生成View视图对象，最后对View进行渲染将处理结果通过页面展现给用户
- `View`：视图，具体的页面

### Spring MVC相关注解

- `@RequestParam`：用于接收URL中的键值对，可以传入参数，此时参数将作为key的名字
- `@RequestBody`：用于接收前端传递的JSON字符串数据，一般用于POST请求，只能有一个被标记为`@RequestBody`的方法参数

## Spring Security

Spring Security是一个针对Spring项目的安全管理框架，应用程序的两个主要区域是“认证”和“授权”：

- 认证（Authentication）：验证用户名与密码，以确认用户身份的过程
- 授权（Authorization）：授予对系统的访问权限的过程

### `SecurityFilterChain`

***TODO***

#### `HttpSecurity` & `WebSecurity`

- `HttpSecurity`：基于网络请求的安全访问控制，在Servlet中网络请求主要在Filter中，这部分主要配置的是HTTP请求流转过程中的安全
- `WebSecurity`：基于Servlet的Web应用的安全访问控制，Spring Security与Spirng MVC紧密集成，在此可以进行整体的配置，优先级最高

### RBAC权限模型

RBAC（Role-Based Access Control），基于角色的访问控制，是一种广泛应用的权限管理模型，它将用户的角色授权与权限关联起来，在管理上更加灵活，易于扩展与维护。

RBAC主要包括以下三个核心概念：

- 角色（Role）：一组具有相似功能或权限的集合，角色是对权限的抽象，用于对用户进行授权
- 权限（Permission）：能够执行的操作或访问的资源
- 用户（User）：使用系统的个体

RBAC中通常使用以下三种方式进行权限控制：

- 基于角色的权限控制：将一组权限授予角色，然后将角色分配给用户，用户的权限通过其所属的角色来控制
- 基于资源的权限控制：将权限授予特定的资源
- 基于用户的权限控制：将权限直接授予用户，通常用于在角色之外对某些特定权限进行授权

## Spring Boot

Spring Boot是一个快速开发框架，快速地将一些常用的第三方依赖整合（通过Maven父子工程的形式），简化XML配置，全部采用注解形式，最终以Java应用程序进行执行。

### 自动配置

`@SpringBootApplication`是以下三个注解的合注解：

- `@Configuration`：使用JavaConfig进行配置，等同于使用XML
- `@ComponentScan`：开启注解扫描，扫描当前类所在的包下的所有标注`@Component`及其子类注解的类加载到IOC容器中
- `@EnableAutoConfiguration`：开启自动配置功能，自动载入应用程序所需要的所有默认配置

开启自动注解后，Spring Boot内部会加载`META-INF/spring.factories`文件的信息，筛选出以EnableAutoConfiguration为key的数据并加载到IOC容器中实现自动配置功能。

[https://zhuanlan.zhihu.com/p/55637237](https://zhuanlan.zhihu.com/p/55637237)

#### spring-boot-starter

启动器，能够简化繁杂的配置，可以自动合并依赖，将其统一集成到一个starter中，后续只需要在Maven或Gradle中引入依赖即可，Spring Boot会自动扫描需要加载的信息并启动响应的默认配置，即spring-boot-starter提供了以下功能：

- 整合模块所需要的依赖并统一集成到starter中
- 提供默认配置并允许调整
- 提供自动配置类对模块内的Bean进行自动装配，注入到Spring容器中

利用starter实现自动化配置需要两个条件：Maven依赖和配置文件，自动化配置流程：

1. 引入starter相关Maven依赖；
2. Spring Boot启动时寻找starter Jar中`resources/META-INF/spring.factories`文件，根据`spring.factories`文件中的配置，找到需要自动配置的类

相关注解：

|注解|解释|
|---|---|
|`@Configuration`|用于定义配置类|
|`@ConditionOnBean`|当容器中存在指定的Bean时才注册|
|`@ConditionOnMissingBean`|当容器中无指定的Bean时才注册|
|`@ConditionOnClass`|表示只有指定的类在classpath上才注册|
|`@ConditionOnMissingClass`|当容器中无指定的类时才注册|
|`@ConditionOnProperty`|指定参数的值符合要求时才注册|
|`@ConfigurationProperties`|用于获取配置文件中的属性定义并绑定到Java Bean属性中|
|`@EnableConfigurationProperties`|让使用了`@ConfigurationProperties`注解的类生效并将该类注入到IOC容器中|

关于`spring.factories`：[https://blog.csdn.net/nsplnpbjy/article/details/106465719](https://blog.csdn.net/nsplnpbjy/article/details/106465719)

#### 自定义starter

Maven需要引入以下依赖：

```xml
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-autoconfigure</artifactId>
</dependency>
<!-- 可选，加入此依赖主要是打包时自动生成配置元信息文件META-INF/spring-configuration-metadata.json并放入Jar中，方便使用者了解到一些配置元信息 -->
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-configuration-processor</artifactId>
</dependency>
```

### bootstrap & application

关于bootstrap：

- 用于应用程序上下文引导阶段，由父Spring ApplicationContext加载，比application优先加载
- 用于程序引导时执行，应用于更加早期的配置信息读取，可理解为系统级别的一些参数配置，这些参数一般不会变动，一旦bootstrap被加载则内容不会被覆盖
- 用于从额外的资源来加载配置信息
- 可在本地外部配置文件中解密属性
- 典型应用场景：

    - 使用Spring Cloud Config Server配置中心时需要在bootstrap中指定`spring.application`配置文件中`name`和`spring.cloud.config.server.git.uri`，添加连接到配置中心的配置属性来加载外部配置中心的配置信息
    - 一些固定的不能被覆盖的属性
    - 一些加解密的场景
关于application：

- 用于Spring Boot项目的自动化配置
- 用来定义应用级别的，应用程序特有配置信息，用来配置后续各个模块中需使用的公共参数等

## Spring Cloud

Spring Cloud是一系列框架的集合，利用Spring Boot的开发便利性巧妙地简化了分布式系统基础设施的开发，如服务注册发现、配置中心、消息总线、负载均衡、断路器、数据监控等，可以用Spring Boot的开发风格做到一键启动和部署。



