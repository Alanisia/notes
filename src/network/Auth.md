# 认证、鉴权与授权

- 认证（identification）：确认声明者的身份
- 鉴权（authentication）：声明者所声明的身份权利之真实性进行鉴别确认的过程
- 授权（authorization）：获取用户的委派权限

## Session和Cookie

由于HTTP的无状态性，为了使某个域名下的所有网页能够共享某些数据，出现了session和cookie，客户端访问服务器的流程如下：

1. 客户端发送一个HTTP请求到服务端；
2. 服务端接收到请求后，建立一个session，并发送一个HTTP响应到客户端，响应头包括Set-Cookie头部，该头部包含了sessionId；
3. 在客户端发起的第二次请求，加入服务端给了Set-Cookie，浏览器会自动在请求头中添加cookie；
4. 服务端接收请求，分解cookie，验证信息，核对成功后返回响应给客户端。

## Token

Token（令牌）认证方式类似于临时的证书签名，并且是一种服务端无状态的认证方式，非常适合于Restful API的场景，所谓无状态即是服务端不会保存身份认证相关的数据。

Token组成：

- uid，用户唯一身份标识
- time，当前时间的时间戳
- sign，签名，使用hash/encrypt压缩成定长的十六进制字符串，以防止第三方恶意拼接
- 固定参数（可选），将一些常用的固定参数加入到token中是为了避免重复查库

Token存放：在客户端一般存放于`localStorage`，`cookie`，`sessionStorage`中；在服务器一般存放于数据库中。

Token认证流程：

1. 用户登录，成功后服务器返回token给客户端；
2. 客户端收到数据后保存在客户端；
3. 客户端再次访问服务器，将token放入headers中；
4. 服务器端采用过滤器校验，校验成功则返回请求数据，校验失败则返回错误码。

_注：Token可以抵抗CSRF攻击，攻击者无法访问用户的token，浏览器不会自动添加到headers里，提交的表单无法通过服务器过滤，无法形成攻击。_

> 参考资料
>
> - Session和JWT：[https://blog.csdn.net/JustKian/article/details/94002865](https://blog.csdn.net/JustKian/article/details/94002865)
> - 单点登录：[https://zhuanlan.zhihu.com/p/66037342](https://zhuanlan.zhihu.com/p/66037342)
> - Cookie, Session和JWT：
>     - [https://juejin.cn/post/7111349594625146887](https://juejin.cn/post/7111349594625146887)
>     - [https://cloud.tencent.com/developer/article/1683290](https://cloud.tencent.com/developer/article/1683290)

### JWT (JSON Web Token)

***TODO***

## OAuth

一个验证授权的开放标准，所有人都有基于这个标准实现自己的OAuth。OAuth主要有OAuth1.0a和OAuth2.0两个版本，二者完全不同且不兼容。

出现OAuth的原因：

在OAuth出现之前使用的是HttpBasic认证方式，即用户输入用户名密码的方式进行验证，这种形式并不安全。OAuth的出现即是为了解决访问资源的安全性与灵活性。OAuth使得第三方应用对资源的访问更加安全。

### OAuth 2.0

> 参考资料：
>
> - [OAuth 2.0 Framework (RFC 6749)](https://datatracker.ietf.org/doc/html/rfc6749)
> - [https://www.ruanyifeng.com/blog/2014/05/oauth_2_0.html](https://www.ruanyifeng.com/blog/2014/05/oauth_2_0.html)

名词定义：

- Third-party Application：第三方应用程序（客户端）
- HTTP Service：HTTP服务提供商
- Resource Owner：资源所有者（用户）
- User Agent：用户代理（浏览器）
- Authorization Server：认证服务器（服务提供商专门用来处理认证的服务器）
- Resource Server：资源服务器（服务提供商存放用户生成的资源的服务器，与认证服务器可以是同一台也可以不同）

思路：OAuth在客户端与服务提供商之前设置了一个授权层，客户端不能直接登录服务提供商，只能登录授权层，以此将用户与客户端区分开来。客户端登录授权层所用的令牌与用户的密码不同，用户可在登录时指定授权层令牌的权限范围及有效期。客户端登录授权层后，服务提供商根据令牌的权限范围和有效期向客户端开放用户存储的资料。

#### 运行流程

```
+------+                             +-------------+
|      |--(1)-Authorization Request->|   Resource  |
|      |<-(2)--Authorization Grant---|    Owner    |
|      |                             +-------------+
|      |
|      |                             +-------------+
|      |--(3)--Authorization Grant-->|Authorization|
|Client|<-(4)-----Access Token-------|   Server    |
|      |                             +-------------+
|      |
|      |                             +-------------+
|      |--(5)-----Access Token------>|  Resource   |
|      |<-(6)---Protected Resource---|   Server    |
|      |                             +-------------+
+------+
```

1. 用户打开客户端以后，客户端要求用户给予授权
2. 用户同意给予客户端授权
3. 客户端使用上一步获得的授权向认证服务器申请令牌
4. 认证服务器对客户端进行认证后，确认无误，同意发放令牌
5. 客户端使用令牌向资源服务器申请访问资源
6. 资源服务器确认令牌无误，同意向客户端开放资源

#### 客户端的授权模式

客户端必须得到用户的授权才能获得令牌，OAuth2.0定义了四种授权模式：

- 授权码模式（authorization code）
- 简化模式（implicit）
- 密码模式（resource owner password credentials）
- 客户端模式（client credentials）

#### 更新令牌

如果用户访问时，客户端的访问令牌已经过期，则需要使用更新令牌申请一个新的访问令牌。

客户端发出更新令牌的HTTP请求，包含下列参数：

- `grant_type`：使用的授权模式，此处值固定为`refreshtoken`，必选项
- `refresh_token`：表示早前收到的更新令牌，必选项
- `scope`：表示申请的授权范围，不可以超出上一次申请的范围，如果省略该参数，则表示与上一次一致

## SSO（单点登录）

单点登录（Single-Sign-On），指在多系统应用群中登录一个系统，便可以在其他所有系统中得到授权而无需再次登录。

原理：

- 登录

    SSO需要一个独立的认证中心，只有认证中心能接受用户的用户名密码等安全信息，其他系统不提供登录入口，只接受认证中心的间接授权。

    间接授权通过令牌实现，SSO认证中心验证用户的用户名密码没有问题则创建授权令牌，在接下来的跳转过程中授权令牌作为参数发送给各个子系统，子系统拿到令牌即得到授权，可以借此创建局部会话，局部会话登录方式与单系统的登录方式相同。

    ![SSO登录](https://p9-juejin.byteimg.com/tos-cn-i-k3u1fbpfcp/b7ebf3c51d5e410e9d059b036809d2d0~tplv-k3u1fbpfcp-zoom-in-crop-mark:4536:0:0:0.awebp?)

    用户登录成功以后，会与SSO认证中心及访问子系统建立会话，用户与SSO认证中心建立的会话称为全局会话，用户与各个子系统建立的会话称为局部会话，局部会话建立以后用户访问子系统受保护资源将不再通过SSO认证中心，全局会话与局部会话存在以下约束关系：

    - 局部会话存在，全局会话一定存在
    - 全局会话存在，局部会话不一定存在
    - 全局会话销毁，局部会话必须销毁

- 注销

    在一个子系统注销，所有子系统的会话都将被销毁。SSO认证中心一直监听会话的状态，一旦全局会话销毁，监听器将通知所有注册系统执行注销操作。

    ![SSO注销](https://p1-juejin.byteimg.com/tos-cn-i-k3u1fbpfcp/e998670591ec4200940c52eca2db923b~tplv-k3u1fbpfcp-zoom-in-crop-mark:4536:0:0:0.awebp?)