# 缓存

## Redis

***TODO***

### 数据类型

|数据类型|可存储的值|操作|应用场景|
|---|---|---|---|
|String|字符串、整数或浮点数|对整个字符串或字符串的其中一部分执行操作<br>对整数和浮点数执行自增或自减操作|简单的键值对缓存|
|List|列表|从两端压入或弹出元素<br>对单个或多个元素进行修剪，只保留一个范围内的元素|存储列表型数据结构|
|Set|无序集合|添加、获取、删除单个元素<br>检查一个元素是否存在于集合中<br>计算交集、并集、差集<br>从集合中随机获取元素|交集、并集、差集的操作|
|Hash|包含键值对的无序散列表|添加、获取、移除单个键值对<br>获取所有键值对<br>检查某个键是否存在|结构化的数据，如对象|
|ZSet|有序集合|添加、获取、删除元素<br>根据分值范围或成员来获取元素<br>计算一个键的排名|去重，排序|

### 客户端常用操作命令

- Key操作命令

    - 获取所有key：`keys <pattern>`，如`keys *`
    - 获取key总数：`dbsize`
    - 查询key是否存在：`exists key [key...]`，查询多个时返回存在的个数
    - 删除key：`del key [key...]`，删除多个时返回成功删除的个数
    - 查询key类型：`type key`
    - 移动key：`move key db`
    - 查询key的生命周期：`ttl key`（秒），`pttl key`（毫秒），返回-1表示永不过期
    - 设置key永不过期：`persist key`
    - 更改key名称：`rename key newkey`
- String操作命令

    - 存放键值：`set key value [EX seconds] [PX milliseconds] [NX|XX]`（NX：如果key不存在则建立，XX：如果key存在则修改其值；也可直接使用`setnx`/`setex`命令）
    - 获取键值：`get key`
    - 值递增/递减（数字类型，否则报错）：`incr key`/`decr key`（一次递增/递减N可使用`incrby`/`decrby`命令，浮点型数据采用`incrbyfloat`/`decrbyfloat`命令）
    - 批量存放键值：`mset key value [key value ...]`
    - 批量获取键值：`mget key [key...]`
    - 获取值长度：`strlen key`（Redis使用UTF-8编码，汉字将返回3字节）
    - 追加内容（向键值尾部添加）：`append key value`
    - 获取部分字符：`getrange key start end`
- 集合操作命令

    - 存储值：`sadd key member [member...]`
    - 获取所有元素：`smembers key`
    - 随机获取元素：`sraddmember langs count`
    - 判断集合是否存在元素：`sismember key member`
    - 获取集合元素个数：`scard key`
    - 删除集合元素：`srem key member [member...]`
    - 弹出元素：`spop key [count]`
- 有序集合操作命令

    - 存储值：`zadd key [NX|XX] [CH] [INCR] score member [score member ...]`
    - 获取元素分数：`zscore key member`
    - 获取指定排名范围排名：`zrange key start stop [WITHSCORES]`（`WITHSCORES`将返回分数）
    - 获取指定分数范围排名：`zrangebyscore key min max [WITHSCORES] [LIMIT offset count]`
    - 增加指定元素分数：`zincrby key increment member`
    - 获取集合元素个数：`zcard key`
    - 获取指定范围分数个数：`zcount key min max`
    - 删除指定元素：`zrem key member [member ...]`
    - 获取元素排名：`zrank key member`
- 列表操作命令

    - 存储值（左端/右端）：`lpush|rpush key value [value...]`
    - 索引存储值：`lset key index value`
    - 弹出元素（左端/右端）：`lpop|rpop key`
    - 获取元素个数：`llen key`
    - 获取列表元素：`lrange key start stop`
    - 获取索引：`lindex key index`
    - 删除元素：`lrem key count value`（`count`负数时为从右端删除）
    - 范围删除：`ltrim key start stop`
- 散列操作命令

    - 存放键值（单个）：`hset key field value`
    - 存放键值（多个）：`hmset key field value [field value ...]`
    - 存放键值（不存在）：`hsetnx key field value`
    - 获取字段值（单个）：`hget key field`
    - 获取字段值（多个）：`hget key field [field...]`
    - 获取所有键与值：`hgetall key`
    - 获取所有字段：`hkeys key`
    - 获取所有值：`hvals key`
    - 判断字段是否存在：`hexists key field`
    - 获取字段数量：`hlen key`
    - 递增/递减：`hincrby key field increment`
    - 删除字段：`hdel key field [field...]`

### 应用场景

[https://blog.csdn.net/agonie201218/article/details/123640871](https://blog.csdn.net/agonie201218/article/details/123640871)

- 计数器：***TODO***
- 缓存：***TODO***
- 会话缓存：***TODO***
- 全页缓存：***TODO***
- 查找表：***TODO***
- 消息队列：***TODO***
- 分布式锁实现：***TODO***
- 其他操作：***TODO***

### 缓存失效

**缓存穿透**

使用Redis大部分情况是通过key查询对应的值，加入发送的请求传进来的key是不存在于Redis中的，那么就查不到缓存，会去数据库查询。假如有大量这样的请求，这些请求像”穿透“缓存一样打在数据库上，这种现象唤作缓存穿透。

分析：关键在于传进来的key在Redis中不存在。

解决方案：

1. 把无效的key存进Redis中，如果Redis和数据库都查不到的数据，可以把这个key存进Redis并设置value为null，当下次再通过该key查询时就不会再查数据库，但假如传进来的这个不存在的key每次都是随机的，存进Redis亦无意义；
2. 使用布隆过滤器，若某个key不存在那就一定不存在，若某个key存在那很大可能存在，于是可以在缓存之前再加一层布隆过滤器，在查询时先去布隆过滤器查询key是否存在，若不存在则直接返回。

**缓存雪崩**

当某一个时刻出现大规模的缓存失效的情况，就会导致大量的请求直接打在数据库上面，导致数据库压力巨大，如果在高并发的情况下可能瞬间就会导致数据库宕机。此时如果运维马上重启数据库，马上又会有新的流量打死数据库。

分析：造成缓存雪崩的关键在于在同一时间大规模key失效，有可能是Redis宕机，有可能是采用了相同的过期时间。

解决方案：

1. 在原有的失效时间上加上一个随机值，比如1-5分钟随机，避免因为采用相同的过期时间导致的缓存雪崩；
2. 使用熔断机制，当流量到达一定的阈值时，就直接返回”系统拥挤“之类的提示，防止过多的请求打在数据库上，至少能保证一部分用户是可以正常使用的，其他用户多刷新几次也能得到结果；
3. 提高数据库的容灾能力，可以使用分库分表，读写分离的策略；
4. 为了防止Redis宕机导致缓存雪崩的问题，可以搭建Redis集群，提高Redis的容灾性。

**缓存击穿**

对一个热点的key，有大并发集中对其进行访问，突然该key失效导致大并发全部打在数据库上，导致数据库压力剧增。

分析：关键在于某个热点key失效了，导致大并发集中打在数据库上。可以从两方面解决：

- 是否可以考虑热点key不设置过期时间；
- 是否可以考虑降低打在数据库上的请求数量。

解决方案：

1. 如业务允许，对于热点key可以设置永不过期；
2. 使用互斥锁，如果缓存失效，只有拿到锁才可以查询数据库，降低了在同一时刻打在数据库上的请求，防止数据库打死，当然这样会导致系统的性能变差。

### 持久化

将内存的数据写入磁盘，防止服务宕机导致内存数据丢失。

机制：

1. RDB（默认）

    ***TODO***

2. AOF

    ***TODO***

优缺点：***TODO***

### 过期时间

设置过期时间的命令：`EXPIRE`。

***TODO***

#### 过期键的删除策略

由于缓存的key可以设置过期时间，过期策略讨论的是当缓存的key过期了如何处理的问题。

1. 定时过期 ***TODO***

2. 惰性过期 ***TODO***

3. 定期过期 ***TODO***

### 淘汰策略

***TODO***

### 线程模型

***TODO***

### 事务

***TODO***

### 集群

***TODO***

### 分区

***TODO***



