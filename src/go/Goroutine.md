# Go协程

## CSP模型

[https://zhuanlan.zhihu.com/p/313763247](https://zhuanlan.zhihu.com/p/313763247)

Communicating Sequential Process（通信顺序进程），一种并发编程模型，用于描述两个独立的并发实体通过共享的通讯管道（channel）进行通信的并发模型。Go中并未完全实现CSP的所有理论，仅仅实现了process和channel，process在Go中表现为Goroutine，即实际并发的实体，每个实体通过channel通讯实现数据共享。

## Goroutine

实际并发的实体，底层使用协程（coroutine）实现并发。Go底层选择coroutine的原因是其具有以下特点：

- 运行在用户空间，避免内核态和用户态切换导致的成本
- 可以由语言和框架层进行调度
- 更小的栈空间运行创建大量的实例

### GPM调度

***TODO***

## 信道（Channel）

通过channel，Go实现了通过通信来实现内存共享。Channel是在多个goroutine之间传递数据和同步的重要手段。使用原子函数、读写锁可以保证资源的共享安全，但使用channel更优雅。

声明channel的语法：

```go
chan T   // 声明一个双向通道
chan<- T // 声明一个只能用于发送的通道
<-chan T // 声明一个只能用于接收的通道
```

Channel是一种引用类型，在被初始化之前的值是`nil`，可以通过`make()`函数初始化。可以向其传递一个整型值，代表channel缓冲区的大小，构造出来的是缓冲型的channel，不传或传0则构造一个非缓冲型的channel。

非缓冲型channel无法缓冲元素，连续向非缓冲channel发送多个元素，如果没有接收则第一次发送会被阻塞，即发送方和接收方要同步就绪，只有在双方都准备好的情况下数据才能在两者间传输；缓冲型channel在缓冲槽可用情况下（即有剩余容量）发送和接收都可以顺利进行，否则操作的一方仍会被挂起直到出现相反操作才会被唤醒。

对channel的发送和接收操作会在编译期间转换成底层的发送和接收函数。