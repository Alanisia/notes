# Go

## 基本类型

|类型|长度（字节）|默认值|说明|
|---|---|---|---|
|`bool`|1|`false`||
|`byte`|1|0|`uint8`|
|`rune`|4|0|Unicode Code Point, `int32`|
|`int`, `uint`|4/8|0|32/64位|
|`int8`, `uint8`|1|0|-128~127/0~255（C: `short`）|
|`int16`, `uint16`|2|0|-32768~32767/0~65535|
|`int32`, `uint32`|4|0|-21亿~21亿/0~42亿|
|`int64`, `uint64`|8|0||
|`float32`|4|0.0||
|`float64`|8|0.0||
|`complex64`|8|||
|`complex128`|8|||
|`uintptr`|4/8||以存储指针的`uint32`或`uint64`整数|
|array|||值类型|
|`string`||`""`|值类型|
|`struct`|||值类型|
|slice||`nil`|引用类型|
|`map`||`nil`|引用类型|
|channel（`chan`）||`nil`|引用类型|
|`interface`||`nil`|接口|
|function（`func`）||`nil`|函数|

***TODO***

## 数组

***TODO***

## Slice（切片）

切片的底层结构：

```go
type slice struct {
  array unsafe.Pointer // 指向数组的指针
  len int // 长度
  cap int // 容量
}
```

**创建切片**

- `make()`：***TODO***
- 空切片：***TODO***

**切片扩容**

***TODO***

**切片拷贝**

***TODO***

## 指针

***TODO***

## Map

无序键值对的集合，通过key进行快速检索，可以像迭代数组和切片那样进行迭代，但由于map无序，所以无法决定其返回顺序。

定义map：

- `make()`：`map_v := make(map[key_type]value_type)`
- `map`：`var map_v map[key_type]value_type`

_若不初始化map则会创建一个nil map，nil map不能用于存放键值对。_

`delete(map, key)`：删除键值对

## 接口

`interface`是一组方法的集合，这些方法可以被任意类型实现。`interface`是一种抽象类型，相对于抽象类型的是具体类型（如`int`，`string`等）。具体使用中可以自定义结构体，并提供特定接口的方法（隐式实现接口），如：

```go
type MyInterface interface {
  Print(value string)
}

type MyStruct struct {}

// 此时MyStruct实现了MyInterface接口
func (ms *MyStruct) Print(value string) {
  println(value)
}
```

函数参数为接口类型时可以传入实现了该接口的结构体对象：

```go
func TestFunc(mi MyInterface) {
  mi.Print("Hello world!")
}

func main() {
  ms := new(MyStruct)
  TestFunc(ms)
}
```

如果一个类型实现了接口，其必须实现该接口的所有方法。接口的实现方法：值类型实现和指针类型实现，如果一个类型的指针类型实现了一个接口，则它的值类型也将隐式实现该接口。

接口由两部分组成：类型和值，类型表示实现该接口的类型，值表示该类型的值。将一个类型的值赋值给一个接口类型的变量时，编译器会将该值的类型和值分别保存在接口变量中。当通过接口变量调用一个方法时，编译器会根据类型和值查找该方法并调用之。

### `interface{}`

空接口，不包含任何方法，任何类型都实现了该接口，可以将任何类型的值赋值给一个空接口类型的变量，也即空接口类型的变量可以存储任何类型的值。

### 使用技巧

- 类型断言

  一种将接口类型的值转化为其他类型的方式，可以用于判断一个接口类型的值是否是一个特定类型，或将一个接口类型的值转换为一个特定类型，语法：

  ```go
  // value表示转换后的值，ok表示是否转换成功
  value, ok := interface.(type)

  // value表示interface的值，Type1和Type2表示不同类型
  switch value := interface.(type) {
    case Type1:
      // ...
    case Type2:
      // ...
    // ...
    default:
      // ...
  }
  ```

- 接口组合

  一种将多个接口组合成一个接口的方式，可以将不同接口组合成一个更大、更复杂的接口，以满足不同需求。语法：

  ```go
  type BigInterface interface {
    Interface1
    Interface2
    Interface3
    // ...
  }
  ```

## 面向对象

三大基本特征：

- 封装：隐藏对象的属性和实现细节，仅对外提供公共访问方式
- 继承：使得子类具有父类的属性和方法或者重新定义、追加属性和方法等
- 多态：不同对象中同种行为的不同实现方式

Go使用结构体对属性进行封装。相关概念：

- 方法：作用在接收者上的一个函数，接收者是某种类型的变量。方法格式：

  ```go
  func (recv receiver_type) method(parameter_list) (return_value_list) {}
  ```

- 访问权限：通过字母大小写控制可见性，大写字母开头表示可被其他包访问和使用，小写字母开头表示仅限包内使用

- 继承：在结构体内部内嵌匿名类型，此时匿名字段上的方法晋升成外层类型的方法

  > 匿名类型：无显式的变量名，如
  >
  > ```go
  > type Class struct {
  >   Parent
  > }
  > ```

- 多态：可以使用接口实现

## 泛型

_注：1.18版本之后支持泛型。_

***TODO***

## Defer

`defer`语句会将其后面跟随的语句进行延迟处理，在`defer`归属的函数即将返回时，将延迟处理的语句按`defer`定义的逆序进行执行。

用途：非常适合用于处理资源释放问题，如：资源清理、文件关闭、解锁和记录时间等。

### Defer执行时机

`return`底层操作步骤：

1. 给返回值赋值
2. 执行`RET`指令

`defer`底层操作步骤：

1. 给返回值赋值
2. 运行`defer`语句
3. 执行`RET`指令

## 错误处理

`error`接口
```go
type error interface {
  Error() string
}
```

只要实现了`Error()`方法就是实现了`error`接口。据此可自定义error。

### Panic

Panic能够改变程序的控制流，调用`panic()`后会立即停止执行当前函数的剩余代码并在当前goroutine中递归执行调用方的`defer`。

Panic（运行时恐慌）只会在程序运行时才抛出来的异常，在panic被抛出之后，若未在程序中添加任何保护措施，程序会打印出panic的详情并终止运行。如果一个panic是无意间引发的，其中的值只能由Go语言运行时指定，但当使用`panic()`有意引发一个panic时，可以自行指定其包含的值。

嵌套崩溃：`panic()`可以被嵌套调用，多次调用`panic`也不会影响`defer`的正常运行：

```go
func main() {
  defer fmt.Println("in main")
  defer func() {
    defer func() {
      panic("panic again and again")
    }()
    panic("panic again")
  }()
  panic("panic once")
}

// Result:
// $ go run main.go
// in main
// panic: panic once
//  	panic: panic again
//  	panic: panic again
// panic: panic again and again
//
// goroutine 1 [running]:
// ...
// exit status 2
```

**从panic被引发到程序终止运行的大概过程**

某个函数中的某行代码引发一个panic后，初始的panic详情会被建立起来，并且该程序的控制器会立即从此行代码转移到调用其所属函数的那行代码上（调用栈中的上一级），此行代码所属函数的执行随即终止。紧接着，控制权并不会在此有片刻停留，它又会立即转移至上一级的调用代码处，反方向传播直至最外层函数（go函数，对于主协程来说就是`main()`）。但控制器亦不会停留在那里，而是被Go语言运行时系统收回。随后程序崩溃并终止运行，承载程序此次运行的进程亦随之死亡而消失。与此同时，在控制器传播过程中，panic详情会积累和完善，并在程序终止前打印出来。

**panic流程**

1. 编译器将关键字`panic`转换成`runtime.gopanic`
2. 创建新的`runtime._panic`并添加到所在goroutine的`_panic`链表最前面
3. 在循环中不断从当前goroutine的`_defer`链表中获取`runtime._defer`并调用`runtime.reflectcall`运行延迟调用函数
4. 调用`runtime.fatalpanic`中止整个程序

**panic应该包含的值**

***TODO***

### Recover

`recover()`可以中止`panic()`造成的程序崩溃，只能在`defer`中发挥作用。

原理：***TODO***

## CGo

***TODO***

## 包依赖管理

使用`package`关键字定义包，Go规定`main()`须在`main`包下。在同一个包内定义的函数、类型、变量、常量、所有文件下的代码都可随意访问，属于包内公开。

引入包使用`import`关键字。编译器会根据`import`指定的相对路径去搜索包然后导入，这个相对路径是从`GOROOT`或`GOPATH`下的`src`下开始搜索。Go总是先从`GOROOT`出发搜索，再从`GOPATH`列出的路径顺序中搜索，只要一搜到合适的包就停止搜索，当搜索完成仍搜不到包则报错。

如果导入的包重名，则可以进行别名导入，添加一个名称属性为包设置一个别名，如果不想在访问包属性时加上包名则可以为其设置特殊别名`.`，如

```go
import (
  . "fmt"
)
func main() {
  Println() // 此时不能再加上包名“fmt”
}
```

Go要求`import`导包必须在后续中使用，否则报错，若想避免此错误可以在包前加下划线，此为匿名导入，表示无法再访问其内部属性。

> 下划线`_`：又称blank identifier，可以用于赋值时弃值，可用于保留`import`时的包，还可用于丢弃函数的返回值。

导入匿名包时会进行一些初始化操作（如`init()`），如果这个初始化操作会影响当前包，则此匿名导入有意义。

### 远程包

Go集成了从Git上获取远程代码的能力。可以使用`get`子命令进行远程导包。在`import`语句中可以使用，首先从`GOPATH`中搜索路径，搜不到包就调用`go get`远程拉取导入。

### init函数

`init()`经常用来初始化环境、安装包或其他需要在程序启动之前先执行的操作。每个包中都可以定义`init()`，甚至可以定义多个，但建议每个包只定义一个。每次导包时在导入完成后且变量、常量等声明初始化完成后将会调用这个包的`init()`。

对于`main`包，如果定义了`init()`，则其会在`main()`之前执行，之后立即执行`main()`。

### Go Module

模块是存储在文件树中的Go包的集合，其根目录中包含`go.mod`文件，此文件定义了模块的模块路径，它也是用于根目录的导入路径，以及它的依赖性要求，每个依赖性要求都被写为模块路径和特定语义版本。

从1.11始，Go允许在`$GOPATH/src`以外的任何目录使用`go.mod`创建项目。相关命令：

- `go mod init`：初始化`go.mod`
- `go list`：列出依赖
- `go mod edit`：修改依赖
- `go mod tidy`：移除不需要的依赖

## 测试

[https://juejin.cn/post/7131966825474031646](https://juejin.cn/post/7131966825474031646)

Go通过标准库的testing包和Go命令行工具test相关命令，在语言层面提供了一整套全面的测试机制。

- 常规测试

  - 测试源码的文件名以`_test.go`结尾
  - 测试函数的函数名以`Test`开头
  - 函数签名为`func (t *testing.T)`

  通过`go test -run ^TestXX`可以运行测试函数。

- 基准测试：测试函数性能

  基本原理：多次循环调用待测函数，计算平均耗时等指标

  - 测试源码的文件名以`_test.go`结尾
  - 测试函数的函数名以`Benchmark`开头
  - 函数签名为`func (b *testing.B)`

  通过`go test -run=^$ -benchmem -bench ^BenchXX`可以运行测试函数。和常规测试不同，基准测试的日志总是会被打印出来。

- Example：示例代码

  - 源码的文件名以`_test.go`结尾
  - 函数名以`Example`开头：
    - 包/类型/函数/方法Example为：`Example`/`ExampleT`/`ExampleF`/`ExampleT_M`
    - 包/类型/函数/方法多个Example为：`Example_suffix`/`ExampleT_suffix`/`ExampleF_suffix`/`ExampleT_M_suffix`
  - 函数签名：`func ()`
  - 对Example的输出进行校验，在函数体最后添加如下注释：

    ```go
    // 一般输出
    func ExampleHello() {
      println("hello, and")
      println("goodbye")
      // Output: 
      // hello, and
      // goodbye
    }

    // 无序输出
    func ExamplePerm() {
      for _, value := range Perm(5) {
        fmt.Println(value)
      }
      // Unordered output: 4
      // 2
      // 3
      // 1
      // 0
    }
    ```

  通过`go test -run ^ExampleXX`可以运行测试函数。Example函数除了可以用`go test`进行测试外，还可以通过`go doc`命令生成到Go doc文档中。

- Fuzzing测试（1.18新特性）

  - 测试源码的文件名以`_test.go`结尾
  - 测试函数名以`Fuzz`开头
  - 函数签名为`func (f *testing.F)`

  通过`go test -fuzz=^FuzzXX -fuzztime 2s -run ^$`可以运行测试，失效的case将写入`testdata/fuzz`目录中。
