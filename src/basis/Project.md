# 项目

## 项目管理

### Git

开源的分布式版本控制系统，用C语言编写，用于敏捷高效地处理任何或大或小的项目。

与SVN区别：

- Git是分布式版本控制系统，SVN是集中版本控制系统
- Git属于第三代版本控制系统，SVN属于第二代
- Git把内容按照元数据方式存储，而SVN按照文件存储；所有的资源控制系统都是把文件的元信息隐藏在一个文件夹里
- 分支不同，SVN分支就是版本库的另外一个目录
- Git无全局版本号，SVN有
- Git内容完整性优于SVN，Git内容存储时使用SHA-1哈希算法，能够确保代码内容的完整性，确保在遇到磁盘故障和网络问题时降低对版本库的破坏
- Git支持离线提交，SVN只能在线提交
- Git的push/pull操作更快，SVN的push/pull操作较慢

### 基本概念

- 工作区：项目工作目录
- 暂存区：一般存放在`.git`目录下的`index(.git/index)`文件下，有时将暂存区称为索引
- 版本库：工作区的隐藏目录`.git`
- 远程仓库：托管在Internet或其他网络中的项目的版本库，供多人分布式开发
- 分支：***TODO***
- 节点：***TODO***

## 项目构建

### Maven

Maven是使用项目对象模型(POM)的概念，可以通过一小段描述信息来管理项目的构建，报告和文档的软件项目管理工具。

Maven仓库是基于简单文件系统存储的，集中化管理Java API资源/构件的一个服务。仓库中的任何一个构件都有其唯一的坐标，根据该坐标可以定义其在仓库中的唯一存储路径。所有Maven项目使用构件的方式是相同的，项目构建完毕后生成的构件也可以安装或部署至仓库中，供其他项目使用。Maven仓库分为本地仓库和远程仓库。

Maven工程类型：

1. POM：逻辑工程，用在父级工程或聚合工程中，用来做Jar包的版本控制；
2. JAR：将会打包成Jar包使用，即常见的Java工程；
3. WAR：将会打包成War包，部署在服务器上的工程

#### 常用命令

- compile：编译
- package：编译、打包
- install：编译、打包并安装到本地仓库
- deploy：部署
- clean：清除已编译信息，即删除工程中的target目录

#### 生命周期

Maven有三套生命周期：

1. clean周期：主要用于清理上一次构建的文件，即删除target目录（调用的插件任务为maven-clean-plugin:clean）
2. 默认周期
    主要阶段包含：
    1. process-resources：默认处理src/resources/下的文件，将其输出到classpath中（调用的插件任务为maven-resources-plugin:resources）
    2. compile：编译src/main/java下的java文件，生成class文件（调用的插件任务为maven-compile-plugin:compile）
    3. process-test-resources：默认处理src/test/resources/下的文件，将其输出到测试的classpath中（调用的插件任务为maven-resources-plugin:testResources）
    4. test-compile：编译src/test/java下的java文件，生成class文件（调用的插件任务为maven-compile-plugin:testCompile）
    5. test：运行测试用例
    6. package：打包构件，生成jar包（调用的插件任务为maven-jar-plugin:jar）或war包
    7. install：安装至本地仓库
    8. deploy：部署至远程仓库
3. site周期
    主要阶段包含：
    1. site：产生项目的站点文档；
    2. site-deploy：将项目的站点文档部署至服务器

#### 依赖传递

- `<scope>`：用于控制依赖的使用范围，指定当前包的依赖范围和依赖的传递性，即指定哪些依赖在哪些classpath中可用
    
    - `compile`：默认值，所有阶段都生效，打包时会包含该依赖
    - `test`：仅测试阶段生效，如单元测试相关依赖
    - `runtime`：仅在测试和运行阶段生效，编译时不被使用，如JDBC驱动
    - `provided`：仅在编译和测试阶段生效，不会参与打包（运行环境不需要或已经具备），不具有传递性
    - `system`：依赖项来自本地文件，使用时需配合`systemPath`属性

    依赖的传递性：假如有A、B、C三依赖，B依赖A，C依赖B，当B通过`test`或`provided`依赖A时，C不依赖A；当B通过`compile`或`runtime`依赖A时，C依赖A。
- `<optional>`：默认值为`false`，此时具有依赖传递性；反之则不具备依赖传递性，常与`runtime`scope一起使用，合理利用该值可减少Jar大小

#### BOM

材料清单（Bill of Materials），罗列了一个工程的所有依赖和其对应的版本，该文件一般被其他工程使用，当其他工程引用BOM中罗列的Jar包时，不用显式指定具体之版本，会自动使用BOM对应的Jar版本。

通过`<parent>`可以引用BOM，如果需要使用多个BOM可以通过`<dependencyManagement>`引用：

```xml
<!-- with parent -->
<parent>
    <!-- groupId, artifactId, version -->
    <relativePath><!-- pom path; lookup parent from repositories --></relativePath>
</parent>

<!-- with dependencyManagement -->
<dependencyManagement>
    <dependencies>
        <dependency>
            <!-- groupId, artifactId, version -->
            <type>pom</type>
            <scope>import</scope>
        </dependency>
    </dependencies>
</dependencyManagement>
```

**版本冲突规则**

当出现版本冲突时，具体使用哪个版本的优先顺序：

1. 直接在当前工程中显式指定的版本
2. `<parent>`中配置的父工程使用的版本
3. 在当前工程中通过`<dependencyManagement>`引入的BOM清单中的版本，当引入的多个BOM都有对应的Jar时先引入的BOM生效
4. 上述三处皆无配置时启用依赖调解（dependency mediation）

> **依赖调解**
>
> 当有两个依赖路径，依赖到同一个Jar的不同版本时，最短路径的版本生效，如：`A->B->C->D 1.4`和`A->E->D 1.0`，最终使用D的1.0版本。

#### 插件解析

***TODO***

**打包插件**

三种打包方式：

- assembly：自定义打包结构，可定制依赖项
    
    使用`maven-assembly-plugin`插件，使用方式：

    ```xml
    <plugin>
        <!-- groupId, artifactId, version -->
        <executions>
            <execution>
                <id><!-- id --></id>
                <phase>package</phase>
                <goals>
                    <goal>single</goal>
                </goals>
            </execution>
        </executions>
        <configuration>
            <!-- 配置描述符文件 -->
            <descriptor>src/main/assembly/assembly.xml</descriptor>
            <!-- Maven预置的配置描述符 -->
            <descriptorRefs>
                <descriptorRef>jar-with-dependencies</descriptorRef>
            </descriptorRefs>
        </configuration>
    </plugin>
    ```

    [配置描述符文件编写](https://blog.51cto.com/u_15294985/5147735)

    配置描述符文件字段：

    |字段|解释|
    |---|---|
    |id|添加到打包文件名的标识符|
    |formats|打包文件格式，支持zip、tar、tar.gz、tar.bz2、jar、war，可同时定义多个|
    |fileSets/fileSet|用来设置一组文件在打包时的属性|
    |directory|源目录路径|
    |includes/excludes|设定包含或排除哪些文件，支持通配符|
    |fileMode|指定该目录下的文件属性，采用Unix八进制描述法，默认为0644|
    |outputDirectory|生成目录的路径|
    |files/file|指定单个文件，可以通过destName属性来设置与源文件不同的名称|
    |dependencySets/dependencySet|用来设置工程依赖文件在打包时的属性，与fileSets大致相同|
    |dependencySet-unpack|false表示将依赖以原来的Jar形式打包，true表示将依赖解成.class文件的目录结构打包|
    |dependencySet-scope|表示符合哪个作用范围的依赖会被打包，一般写runtime|
- shade（fat Jar）：用于打可执行Jar包

    使用`maven-shade-plugin`插件，可将依赖的Jar打入当前Jar中，可以对依赖Jar重命名。使用方式：

    ```xml
    <plugin>
        <!-- groupId, artifactId, version -->
        <!-- configuration -->
        <executions>
            <execution>
                <phase>package</phase>
                <goals>
                    <goal>shade</goal>
                </goals>
            </execution>
        </executions>
    </plugin>
    ```

    可以使用`<excludes>`排除某些Jar包，详见以上链接。
- jar：默认打包方式，用于打普通Jar包

    使用`maven-jar-plugin`插件：

    ```xml
    <plugin>
        <!-- groupId, artifactId, version -->
        <configuration>
            <archive>
                <manifest>
                    <!-- 是否添加依赖的Jar路径配置 -->
                    <addClasspath>true</addClasspath>
                    <!-- 依赖Jar和生成的Jar存放于同一级目录下 -->
                    <classpathPrefix>lib/</classpathPrefix>
                </manifest>
            </archive>
            <!-- 不打包某些类 -->
            <excludes>*</excludes>
        <configuration>
    </plugin>
    ```

### Gradle

***TODO***

## 持续集成

### Jenkins

***TODO***

### Drone

***TODO***
