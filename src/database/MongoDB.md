# MongoDB

[https://zhuanlan.zhihu.com/p/87722764](https://zhuanlan.zhihu.com/p/87722764)

分布式的基于JSON的NoSQL文档型数据库。一些特性：

- 面向文档存储，基于JSON/BSON可表示灵活的数据结构
- 动态DDL能力，无强Schema约束，支持快速迭代
- 高性能计算，提供基于内存的快速数据查询
- 容易扩展，利用数据分片可以支持海量数据存储
- 丰富的功能集，支持二级索引、强大的聚合管道功能，为开发者量身定做的功能，如数据自动老化、固定集合等
- 跨平台，支持多语言SDK

## 基本模型

下表展示了SQL概念与MongoDB概念的对应关系：

|SQL概念|MongoDB概念|
|---|---|
|database|database|
|table|collection|
|row|document|
|column|field|
|primary key|\_id|
|foreign key|reference|
|view|view|
|index|index|
|join|$lookup|
|transaction|transaction|
|group by|aggregation|

- database：数据库，一个数据库包含多个collection
- collection：集合，可存放多个document，集合结构（Schema）是动态的，不需要预先声明，默认情况下MongoDB并不会对写入的数据作任何Schema校验
- document：文档，由多个field组成，采用JSON/BSON格式表示
- field：字段，支持嵌套的文档或数组，区分大小写，类型固定，在文档中的字段是有序的
- \_id：主键，保证文档唯一性
- reference：引用，由客户端自动进行关联查询、转换的特殊类型
- view：视图，基于集合之上进行动态查询的一层对象，可以是虚拟的也可以是物理的
- $lookup：聚合操作符
- transaction：事务
- aggregation：聚合

## BSON数据类型

MongoDB使用一种扩展式JSON以支持像日期这样的特定数据类型，所支持的类型包括：

|Type|Number|Alias|
|---|---|---|
|32-bit Integer|16|整数|
|64-bit Integer|18|长整数|
|Double|1|浮点数|
|Boolean|8|布尔值|
|String|2|字符串|
|Object|3|对象|
|Array|4|数组|
|Date|9|日期|
|Binary Data|5|二进制数据|
|ObjectId|7|文档ID|
|Null|10|空值|
|Timestamp|17|时间戳|
|Decimal128|19|高精度浮点数|
|Min Key|-1|最小值|
|Max Key|127|最大值|
|Regular Exepression|11|正则表达式|
|JavaScript|13|JavaScript函数|
|JavaScript(with scope)|15|JavaScript with Scope|

## 分布式ID

分布式ID需要保证多个节点上的主键不出现重复。

MongoDB采用ObjectId表示主键类型，数据库中每个文档都拥有一个`_id`字段表示主键，生成规则如下：

```
+----------------------+--------------------------+------------------+---------------+
|4 bytes Unix Timestamp|3 bytes Machine Identifier|2 bytes Process ID|3 bytes Counter|
+----------------------+--------------------------+------------------+---------------+
```

- 4 bytes Unix时间戳
- 3 bytes 机器ID
- 2 bytes 进程ID
- 3 bytes 计数器（初始化随机）

`_id`由客户端生成，可获得更好的随机性且降低服务端负载，如果服务端检测到写入的文档没有包含`_id`字段则会自动生成一个。

## 语法

对于数据的操作命令采用基于JSON/BSON格式的语法。

- 插入：`insert({ ... })`
- 查找：`find({ ... })`
- 更新：`update({ ... })`
- 删除：`remove({ ... })`
- 声明索引：`ensureIndex({ ... })`

MongoDB也可以使用SQL进行查询，需要借助第三方平台实现。

## 索引

基于B+树数据结构，当前MongoDB版本使用wiredTiger作为默认引擎。

***TODO***
