# JDBC

Java数据库连接，用于执行SQL语句的Java API。应用程序可以通过这套API连接到关系型数据库，并使用SQL语句来完成数据库中的增删改查操作。

应用程序使用JDBC访问特定的数据库时，需要与不同的数据库驱动进行连接。因此，JDBC的实现包含三部分：

- JDBC驱动管理器：负责注册特定的JDBC驱动器，主要通过`java.sql.DriverManager`实现
- JDBC驱动器API：由Sun公司负责制定，其中最主要的接口是`java.sql.Driver`
- JDBC驱动器：数据库驱动，由数据库厂商创建，负责与特定的数据库连接，以及处理通信细节

## 常用API

[https://zhuanlan.zhihu.com/p/140885502](https://zhuanlan.zhihu.com/p/140885502)

## Spring Data JDBC

***TODO***
