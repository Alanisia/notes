# ORM

对象关系映射（Object Relational Mapper）是一种为了解决关系型数据库数据与简单对象的映射关系的技术。通过使用描述对象和数据库之间映射的元数据，将程序中的对象自动持久化到关系型数据库中。

## MyBatis

半自动化ORM框架，支持定制化SQL、存储过程及高级映射。可以使用XML或注解来配置和映射原生类型、接口和Java的POJO为数据库中的记录。由于需要自己编写SQL，而SQL依赖数据库，会导致数据库移植性差，不能随意更换数据库。

功能架构：

1. API接口层
2. 数据处理层
3. 基础支撑层

编程步骤：

1. 创建`SqlSessionFactory`
2. 通过`SqlSessionFactory`创建`SqlSession`
3. 通过`SqlSession`执行数据库操作
4. 调用`session.commit()`提交事务
5. 调用`session.close()`关闭会话

工作原理：

1. 读取配置文件
2. 加载映射文件
3. 构造会话工厂
4. 创建会话对象
5. 创建Executor执行器
6. 创建MappedStatement对象
7. 输入参数映射
8. 输出结果映射

### Executor执行器

***TODO***

MyBatis定义了三种基本的执行器：

1. SimpleExecutor
2. ReuseExecutor
3. BatchExecutor

***TODO***

### ${}和#{}的区别

- `#{}`：预处理编译，处理`#{}`时会将其替换为`?`号，调用`PreparedStatement`的`set()`方法来赋值，此方法可以有效防止SQL注入，提高系统安全性
- `${}`：字符串替换，将`${}`替换成变量的值

### 模糊查询

***TODO***

### 动态SQL

在XML映射文件内以标签的形式编写动态SQL，完成逻辑判断和SQL动态拼接。MyBatis提供了9种动态SQL标签：

- `<trim>`
- `<if>`
- `<foreach>`
- `<set>`
- `<where>`
- `<choose>`
- `<when>`
- `<otherwise>`
- `<bind>`

### 分页

- 直接编写SQL进行分页
- 使用`RowBounds`对象
- 使用分页插件：实现MyBatis提供的接口，拦截待执行的SQL并进行重写

### 插件

***TODO***

### 缓存

1. 一级缓存（本地缓存）

    基于PerpetualCache的HashMap本地缓存，其存储作用域为`session`，一个SqlSession对象会使用一个Executor对象来完成会话操作，Executor对象会维护一个Cache缓存以提高查询性能。当`session`flush或close之后，该`session`中的所有cache就将清空。默认打开一级缓存。

2. 二级缓存（全局缓存）

    与一级缓存机制相同，默认也采用PerpetualCache，HashMap存储，不同在于其作用域为mapper(namespaces)，并且可自定义存储源，如EhCache；默认不打开二级缓存，使用二级缓存属性类需要实现Serializable序列化接口（用来保存对象状态），可在其映射文件中配置。

    工作机制：如果用户配置了`cacheEnabled=true`，则MyBatis在为SqlSession对象创建Executor对象时对其加上一个装饰者CachingExecutor，这时SqlSession使用CachingExecutor对象来完成请求。CachingExecutor对于查询请求，会先判断该查询请求是否在Application级别的二级缓存中是否有缓存结果，若有则直接返回，若无则交予Executor对象完成查询操作，之后CachingExecutor会将Executor对象返回的结果存入缓存中再返回给用户。

对于缓存更新机制，当某一作用域进行了C/U/D操作后，默认该作用域下所有select中的缓存将被clear。

### 数据源与连接池

数据源分类：

1. UNPOOLED：不使用连接池的数据源
2. POOLED：使用连接池的数据源
3. JNDI：使用JNDI实现的数据源

***TODO***

## Spring Data JPA

***TODO***

一些注解的作用：

- `@Entity`
- `@EntityListeners`：用于指定Entity或者MappedSuperClass上的回调监听类
- `@Table`
- `@Id`
- `@MappedSuperClass`
- `@Query`：在继承自JpaRepository的接口中自定义SQL查询，支持HQL和SQL（默认HQL，使用SQL时需开启`nativeQuery = true`），支持SpEL表达式（[https://blog.csdn.net/yingxiake/article/details/51016234](https://blog.csdn.net/yingxiake/article/details/51016234)）
- `@NamedQuery`：在实体类上定义查询语句，继承JpaRepository接口后会自动生成该方法

### 审计

记录好一件事情的每一个重要阶段，以便后期检查，数据库审计重点侧重于何时创建、何人创建、最后一次修改的时间和最后一次修改者。

Spring Data JPA提供了与审计有关的以上方面的自动填充：

- `@CreatedBy`：何人创建
- `@CreatedDate`：何时创建
- `@LastModifiedBy`：最后一次修改者
- `@LastModifiedDate`：最后一次修改时间

开启审计功能：

启动类加入`@EnableJpaAuditing`注解，当执行create、update、retrieve操作时会自动更新上述4个字段的值。

`AuditingEntityListener`：***TODO***