# Flutter

***TODO***

## Widget

Widget是用户页面的描述，表示了Element的配置信息，本身为不可变，所有它直接声明或继承的变量须为发final类型。

```dart
@immutable
abstract class Widget extends DiagnosticableTree { ... }
```

如果需要给Widget关联一个可变的状态，可以考虑使用StatefulWidget。

- `StatefulWidget`
- `StatelessWidget`
- `RenderObjectWidget`
- `PreferredSizeWidget`
- `ProxyDataWidget`

### Stateful & Stateless

- `StatelessWidget`：无状态组件，更新内容只能通过重新创建和销毁来更新
- `StatefulWidget`：带状态组件，拥有完整之生命周期，可在组件创建完毕后继续更新组件

    生命周期：

    ```
                                +-----------+
                                |createState|
                                +-----------+
                                      |
                                      v
                                 +---------+
                                 |initState|
                                 +---------+
                                      |
                                      v
    +---------------+      +---------------------+         +----------+
    |InheritedWidget|----->|didChangeDependencies|         |reassemble|
    +---------------+      +---------------------+         +----------+
                                      |                          |
                                      v                          v
       +--------+                  +-----+               +---------------+
       |setState|----------------->|build|<--------------|didUpdateWidget|
       +--------+                  +-----+               +---------------+
                                      |
                                      v
                                +----------+
                                |deactivate|
                                +----------+
                                      |
                                      v
                                  +-------+
                                  |dispose|
                                  +-------+

    ```

    1. `createState`
    2. `initState`
    3. `didChangeDependencies`
    4. `build`
    5. `didUpdateWidget`
    6. `deactivate`
    7. `dispose`

## Element

元素树，是Widget在具体位置的实例化，负责控制Widget的生命周期，持有了Widget实例和renderObject实例。

两种基本类型：

- `ComponentElement`：其他Element的宿主，本身不包含RenderObject而由其持有的Element包含
- `RenderObjectElement`：参与layout或者绘制阶段的元素

## State

***TODO***

### Key

***TODO***
